package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_TEM_MarketPlugin extends TEM_MarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.75f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        updateEconomicCommoditiesInCargo(true);

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            FactionAPI startingFaction = null;
            if (market.getMemoryWithoutUpdate().contains("$startingFactionId")) {
                startingFaction = Global.getSector().getFaction(
                market.getMemoryWithoutUpdate().getString("$startingFactionId"));
            }

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            if (startingFaction != null) {
                factionPicker.add(market.getFaction(), 5f);
                factionPicker.add(startingFaction, 5f);
            } else {
                factionPicker.add(market.getFaction(), 10f);
            }

            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(5f, 8f, stability / 10f),
                                                              DS_Util.lerp(2f, 3f, stability / 10f),
                                                              4, factionPicker, submarket,
                                                              submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(5f, 8f, stability / 10f)),
                                            Math.round(DS_Util.lerp(2f, 3f, stability / 10f)),
                                            4, factionPicker);
            }

            addShips(pruneAmount);
        }
        addHullMods(4, 2 + itemGenRandom.nextInt(3));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            countScale = 0.5f;
        }

        // 32
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.COMBAT_SMALL, 10f); //10
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 6f * mediumShipRarity); //6
        rolePicker.add(ShipRoles.COMBAT_LARGE, 3f * largeShipRarity); //3
        rolePicker.add(ShipRoles.COMBAT_CAPITAL, 1f * veryLargeShipRarity); //1

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.75f, 1f, stability / 10f);

        // 4/6/8/10/12/14/16/18 [2/3/4/5/6/7/8/9]
        DS_SubmarketUtils.addShipsForRoles(Math.round((2 + marketSize * 2) * countScale), rolePicker, null, submarket,
                                           submarket.getMarket().getShipQualityFactor());

        if (DSModPlugin.Module_RareShips) {
            return;
        }

        // 4-8/6-10/8-12/10-14/12-16/14-18/16-20/18-22
        boolean hasJesuit = false;
        boolean hasCrusader = false;
        boolean hasPaladin = false;
        boolean hasArchbishop = false;
        for (FleetMemberAPI member : getCargo().getMothballedShips().getMembersListCopy()) {
            switch (member.getHullId()) {
                case "tem_jesuit":
                    hasJesuit = true;
                    break;
                case "tem_crusader":
                    hasCrusader = true;
                    break;
                case "tem_paladin":
                    hasPaladin = true;
                    break;
                case "tem_archbishop":
                    hasArchbishop = true;
                    break;
                default:
            }
        }
        if (!hasJesuit) {
            FleetMemberType type = FleetMemberType.SHIP;
            FleetMemberAPI member = Global.getFactory().createFleetMember(type, "tem_jesuit_Hull");
            member.getRepairTracker().setMothballed(true);
            getCargo().getMothballedShips().addFleetMember(member);
        }
        if (!hasCrusader) {
            FleetMemberType type = FleetMemberType.SHIP;
            FleetMemberAPI member = Global.getFactory().createFleetMember(type, "tem_crusader_Hull");
            member.getRepairTracker().setMothballed(true);
            getCargo().getMothballedShips().addFleetMember(member);
        }
        if (!hasPaladin) {
            FleetMemberType type = FleetMemberType.SHIP;
            FleetMemberAPI member = Global.getFactory().createFleetMember(type, "tem_paladin_Hull");
            member.getRepairTracker().setMothballed(true);
            getCargo().getMothballedShips().addFleetMember(member);
        }
        if (!hasArchbishop) {
            FleetMemberType type = FleetMemberType.SHIP;
            FleetMemberAPI member = Global.getFactory().createFleetMember(type, "tem_archbishop_Hull");
            member.getRepairTracker().setMothballed(true);
            getCargo().getMothballedShips().addFleetMember(member);
        }
    }
}
