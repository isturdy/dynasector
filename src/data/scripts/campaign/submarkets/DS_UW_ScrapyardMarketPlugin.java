package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.DModManager;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;
import data.scripts.util.UW_Defs;
import data.scripts.variants.DS_Database;
import java.util.List;
import java.util.Random;

public class DS_UW_ScrapyardMarketPlugin extends UW_ScrapyardMarketPlugin {

    private static void styxAddShipsForRoles(int maxTotal, WeightedRandomPicker<String> rolePicker,
                                             WeightedRandomPicker<FactionAPI> factionPicker, SubmarketAPI submarket,
                                             float qualityFactor) {
        if (rolePicker.isEmpty()) {
            return;
        }

        Random rand = new Random();
        int curr = submarket.getCargo().getMothballedShips().getMembersListCopy().size();
        int toAdd = maxTotal - curr;
        int added = 0;
        while (added < toAdd) {
            String faction;
            if (factionPicker == null) {
                faction = submarket.getFaction().getId();
            } else {
                faction = factionPicker.pick().getId();
            }
            String role = rolePicker.pick();
            List<ShipRolePick> picks = Global.getSector().getFaction(faction).pickShip(role, qualityFactor, rand);
            if (picks != null && !picks.isEmpty()) {
                for (ShipRolePick pick : picks) {
                    if (!pick.isFighterWing()) {
                        String hullId = DS_Util.getNonDHullId(
                               Global.getSettings().getVariant(pick.variantId).getHullSpec());
                        if (DS_Defs.BANNED_HULLS.contains(hullId)) {
                            continue;
                        }
                    }
                    FleetMemberType type = FleetMemberType.SHIP;
                    String variantId = pick.variantId;
                    if (pick.isFighterWing()) {
                        type = FleetMemberType.FIGHTER_WING;
                    } else {
                        FleetMemberAPI member = Global.getFactory().createFleetMember(type, pick.variantId);
                        variantId = member.getHullId() + "_Hull";
                    }
                    FleetMemberAPI member = Global.getFactory().createFleetMember(type, variantId);
                    member.getRepairTracker().setMothballed(true);

                    int dmods = Math.round(rand.nextFloat() * rand.nextFloat() * 4f);
                    ShipVariantAPI variant = member.getVariant();
                    int dModsAlready = DModManager.getNumDMods(variant);
                    int newDmods = Math.max(0, dmods - dModsAlready);

                    if (newDmods > 0) {
                        DModManager.setDHull(variant);
                    }
                    member.setVariant(variant, false, true);
                    if (newDmods > 0) {
                        DModManager.addDMods(member, true, newDmods, rand);
                    }

                    member.getStatus().setHullFraction(1f - dmods * 0.1f);
                    submarket.getCargo().getMothballedShips().addFleetMember(member);
                }
                added++;
            }
        }
    }

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        for (FleetMemberAPI member : getCargo().getMothballedShips().getMembersListCopy()) {
            /* f = 1 - dmods * 0.2
             dmods = 5 * (1 - f) */
            int dmods = Math.round(5f * (1f - member.getStatus().getHullFraction()));
            ShipVariantAPI variant = member.getVariant();
            int dModsAlready = DModManager.getNumDMods(variant);
            dmods = Math.max(0, dmods - dModsAlready);

            if (dmods > 0) {
                DModManager.setDHull(variant);
            }
            member.setVariant(variant, false, true);
            if (dmods > 0) {
                DModManager.addDMods(member, true, dmods, new Random());
            }
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        updateEconomicCommoditiesInCargo(false);

        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            int index = 0;
            for (String item : UW_Defs.SCRAPYARD_FACTIONS.getItems()) {
                FactionAPI f;
                try {
                    f = Global.getSector().getFaction(item);
                } catch (Exception e) {
                    f = null;
                }
                if (f != null) {
                    factionPicker.add(f, UW_Defs.SCRAPYARD_FACTIONS.getWeight(index));
                }
                index++;
            }

            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(4f, 6f, stability / 10f),
                                                        DS_Util.lerp(2f, 3f, stability / 10f), 1, factionPicker,
                                                        submarket, -1f);
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(4f, 6f, stability / 10f)),
                                            Math.round(DS_Util.lerp(2f, 3f, stability / 10f)), 1, factionPicker);
            }

            addRandomWeapons(Math.max(1, market.getSize()), 2);
            addRandomWings(Math.max(1, market.getSize() - 2), 2);

            addShips(pruneAmount);
        }
        addHullMods(1, 1 + itemGenRandom.nextInt(3));

        cargo.sort();
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 5f);
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f);
        rolePicker.add(ShipRoles.TANKER_SMALL, 20f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f);
        rolePicker.add(ShipRoles.ESCORT_SMALL, 15f * militaryRarity);
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity);
        rolePicker.add(ShipRoles.LINER_SMALL, 1f);

        rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity);
        rolePicker.add(ShipRoles.TANKER_MEDIUM, 10f * mediumShipRarity);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f * mediumShipRarity);
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * militaryRarity * mediumShipRarity);
        rolePicker.add(ShipRoles.ESCORT_MEDIUM, 10f * militaryRarity * mediumShipRarity);
        rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * militaryRarity * mediumShipRarity);
        rolePicker.add(ShipRoles.LINER_MEDIUM, 1f * mediumShipRarity);

        rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity);
        rolePicker.add(ShipRoles.TANKER_LARGE, 5f * largeShipRarity);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity);
        rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * militaryRarity * largeShipRarity);
        rolePicker.add(ShipRoles.CARRIER_MEDIUM, 3f * militaryRarity * largeShipRarity);
        rolePicker.add(ShipRoles.LINER_LARGE, 1f * largeShipRarity);

        rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f * militaryRarity * veryLargeShipRarity);
        rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * militaryRarity * veryLargeShipRarity);

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        int index = 0;
        for (String item : UW_Defs.SCRAPYARD_FACTIONS.getItems()) {
            FactionAPI f;
            try {
                f = Global.getSector().getFaction(item);
            } catch (Exception e) {
                f = null;
            }
            if (f != null) {
                factionPicker.add(f, UW_Defs.SCRAPYARD_FACTIONS.getWeight(index));
            }
            index++;
        }
        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(1f, 1.25f, stability / 10f);

        for (int i = 0; i < 5; i++) {
            int maxTotal = 2 + Math.round(Math.max(1, (marketSize * 6) * countScale));
            int curr = getCargo().getMothballedShips().getMembersListCopy().size();
            int toAdd = maxTotal - curr;
            if (toAdd > 0) {
                if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                    styxAddShipsForRoles(Math.round(Math.max(1, (marketSize * 6) * countScale)),
                                         rolePicker, factionPicker, submarket, -1f);

                } else {
                    addShipsForRoles(Math.round(Math.max(1, (marketSize * 6) * countScale)), rolePicker, factionPicker);
                }
            } else {
                break;
            }
            for (FleetMemberAPI member : getCargo().getMothballedShips().getMembersListCopy()) {
                Float q = DS_Database.variantQuality.get(member.getSpecId());
                if (q != null && q > 0.5f) {
                    getCargo().getMothballedShips().removeFleetMember(member);
                }
            }
        }
    }

}
