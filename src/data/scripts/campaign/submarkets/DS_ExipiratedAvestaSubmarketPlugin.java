package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_ExipiratedAvestaSubmarketPlugin extends ExipiratedAvestaSubmarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        updateEconomicCommoditiesInCargo(true);
        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            factionPicker.add(market.getFaction(), 11f - stability);
            factionPicker.add(submarket.getFaction(), 10f);
            factionPicker.add(Global.getSector().getFaction("exigency"), 3f);
            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(2f, 5f, stability / 10f),
                                                        DS_Util.lerp(1f, 3f, stability / 10f), 3, factionPicker,
                                                        submarket, submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(2f, 5f, stability / 10f)),
                                            Math.round(DS_Util.lerp(1f, 3f, stability / 10f)), 3, factionPicker);
            }

            addRandomWeapons(Math.max(1, market.getSize() - 3), 3);
            addRandomWings(Math.max(1, market.getSize() - 5), 3);

            addShips(pruneAmount);
        }
        addHullMods(4, 1 + itemGenRandom.nextInt(3));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.5f;
            countScale = 0.5f;
        }

        // 155
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 5f); //10
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //10
        rolePicker.add(ShipRoles.TANKER_SMALL, 20f); //10
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //10
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f); //15
        rolePicker.add(ShipRoles.COMBAT_SMALL, 10f * militaryRarity); //20
        rolePicker.add(ShipRoles.ESCORT_SMALL, 10f * militaryRarity); //20
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * militaryRarity * mediumShipRarity); //15
        rolePicker.add(ShipRoles.ESCORT_MEDIUM, 5f * militaryRarity * mediumShipRarity); //15
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //1

        rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //7
        rolePicker.add(ShipRoles.TANKER_MEDIUM, 10f * mediumShipRarity); //7
        rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 1f * mediumShipRarity); //7
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f * mediumShipRarity); //10
        rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * militaryRarity * mediumShipRarity); //10
        rolePicker.add(ShipRoles.LINER_MEDIUM, 1f); //1

        rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
        rolePicker.add(ShipRoles.TANKER_LARGE, 5f * largeShipRarity); //5
        rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
        rolePicker.add(ShipRoles.COMBAT_LARGE, 10f * militaryRarity * largeShipRarity); //10
        rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * militaryRarity * largeShipRarity); //5
        rolePicker.add(ShipRoles.LINER_LARGE, 1f); //1

        rolePicker.add(ShipRoles.COMBAT_CAPITAL, 5f * militaryRarity * veryLargeShipRarity); //5

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        float stability = market.getStabilityValue();
        factionPicker.add(market.getFaction(), 11f - stability);
        factionPicker.add(submarket.getFaction(), 10f);
        factionPicker.add(Global.getSector().getFaction("exigency"), 3f);
        countScale *= DS_Util.lerp(0.75f, 1.1f, stability / 10f);

        // 6/9/12/15/18/21/24/27 [3/4/6/7/9/10/12/13]
        if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((3 + marketSize * 3) * countScale), rolePicker,
                                               factionPicker, submarket, submarket.getMarket().getShipQualityFactor());
        } else {
            addShipsForRoles(Math.round((3 + marketSize * 3) * countScale), rolePicker, factionPicker);
        }
    }
}
