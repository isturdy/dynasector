package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_II_MilitaryIndustrialSubmarketPlugin extends II_MilitaryIndustrialSubmarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(2f / 3f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        updateEconomicCommoditiesInCargo(false);

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);
            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(
                        DS_Util.lerp(4f, 12f, stability / 10f) * getProductivity(),
                        DS_Util.lerp(2f, 4f, stability / 10f) * getProductivity(),
                        3, null, submarket,
                        submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(4f, 12f, stability / 10f) * getProductivity()),
                                            Math.round(DS_Util.lerp(2f, 4f, stability / 10f) * getProductivity()),
                                            3, null);
            }

            addShips(pruneAmount);
        }
        addHullMods(4, Math.round(2 * getProductivity()) + itemGenRandom.nextInt(5));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 63
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //5
        rolePicker.add(ShipRoles.TANKER_SMALL, 3f); //5
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //5
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //25
        rolePicker.add(ShipRoles.ESCORT_SMALL, 10f * militaryRarity); //25
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * mediumShipRarity * militaryRarity); //15
        rolePicker.add(ShipRoles.CARRIER_SMALL, 3f * mediumShipRarity * militaryRarity); //3

        if (marketSize >= 4) { // 140
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 5f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 2f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.ESCORT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * mediumShipRarity * militaryRarity); //13
            rolePicker.add(ShipRoles.COMBAT_LARGE, 10f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f * veryLargeShipRarity * militaryRarity); //3
        }

        if (marketSize >= 5) { // 174.5
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //15
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 4f * veryLargeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //2
        }

        if (marketSize >= 6) { // 198
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //11
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //20
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 5f * veryLargeShipRarity * militaryRarity); //8
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //4
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.1f, stability / 10f) * getProductivity();

        for (int i = 0; i < 5; i++) {
            int maxTotal = Math.round((2 + marketSize * 2) * countScale);
            int curr = getCargo().getMothballedShips().getMembersListCopy().size();
            int toAdd = maxTotal - curr;
            if (toAdd > 0) {
                if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                    DS_SubmarketUtils.addShipsForRoles(maxTotal, rolePicker, null, submarket,
                                                       submarket.getMarket().getShipQualityFactor());
                } else {
                    addShipsForRoles(maxTotal, rolePicker, null);
                }
            } else {
                break;
            }
            if (submarket.getFaction().getId().contentEquals("interstellarimperium")) {
                for (FleetMemberAPI member : getCargo().getMothballedShips().getMembersListCopy()) {
                    if (!member.getSpecId().startsWith("ii_")) {
                        getCargo().getMothballedShips().removeFleetMember(member);
                    }
                }
            }
        }
    }
}
