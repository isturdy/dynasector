package data.scripts.campaign.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactory.MercType;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.fleets.MercAssignmentAI;
import com.fs.starfarer.api.impl.campaign.fleets.MercFleetManager;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.DS_FleetFactory.FleetFactoryDelegate;

public class DS_MercFleetManager extends MercFleetManager {

    @Override
    protected CampaignFleetAPI spawnFleet() {
        final MarketAPI source = pickMarket();
        if (source == null) {
            return null;
        }

        WeightedRandomPicker<MercType> picker = new WeightedRandomPicker<>();
        picker.add(MercType.SCOUT, 10f);
        picker.add(MercType.BOUNTY_HUNTER, 10f);
        picker.add(MercType.PRIVATEER, 10f);
        picker.add(MercType.PATROL, 10f);
        picker.add(MercType.ARMADA, 3f);

        MercType type = picker.pick();

        final float combat;
        final float tanker;
        final float freighter;
        final String fleetType = type.fleetType;
        switch (type) {
            default:
            case SCOUT:
                combat = Math.round(1f + (float) Math.random() * 2f);
                freighter = 0f;
                tanker = 0f;
                break;
            case PRIVATEER:
            case BOUNTY_HUNTER:
                combat = Math.round(3f + (float) Math.random() * 2f);
                freighter = Math.round(0f + (float) Math.random() * 1f);
                tanker = 0f;
                break;
            case PATROL:
                combat = Math.round(9f + (float) Math.random() * 3f);
                freighter = Math.round(0f + (float) Math.random() * 2f);
                tanker = Math.round(0f + (float) Math.random() * 1f);
                break;
            case ARMADA:
                combat = Math.round(10f + (float) Math.random() * 4f);
                freighter = Math.round(0f + (float) Math.random() * 4f);
                tanker = Math.round(0f + (float) Math.random() * 2f);
                break;
        }

        CampaignFleetAPI fleet = DS_FleetFactory.enhancedCreateFleet(
                         Global.getSector().getFaction(Factions.INDEPENDENT), (int) (combat + freighter + tanker),
                         new FleetFactoryDelegate() {
                             @Override
                             public CampaignFleetAPI createFleet() {
                                 return FleetFactoryV2.createFleet(new FleetParams(
                                                 null,
                                                 source,
                                                 Factions.INDEPENDENT,
                                                 null,
                                                 fleetType,
                                                 combat, // combatPts
                                                 freighter, // freighterPts
                                                 tanker, // tankerPts
                                                 0f, // transportPts
                                                 0f, // linerPts
                                                 0f, // civilianPts
                                                 0f, // utilityPts
                                                 0f, // qualityBonus
                                                 -1f, // qualityOverride
                                                 1f, // officer num mult
                                                 0 // officer level bonus
                                         ));
                             }
                         });

        source.getPrimaryEntity().getContainingLocation().addEntity(fleet);
        fleet.setLocation(source.getPrimaryEntity().getLocation().x, source.getPrimaryEntity().getLocation().y);

        MercAssignmentAI ai = new MercAssignmentAI(fleet, source);
        fleet.addScript(ai);

        return fleet;
    }
}
