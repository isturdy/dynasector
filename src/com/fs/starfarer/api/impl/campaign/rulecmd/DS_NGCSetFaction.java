package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.CharacterCreationData;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddFactionOptions.FactionChoice;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * SSP_NGCSetFaction
 */
public class DS_NGCSetFaction extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           Map<String, MemoryAPI> memoryMap) {
        String param = params.get(0).getString(memoryMap);
        String factionId = param.substring("ngcFaction".length());
        memoryMap.get(MemKeys.LOCAL).set("$ngcFaction", factionId, 7);

        FactionChoice faction = FactionChoice.getFactionChoice(factionId);
        memoryMap.get(MemKeys.LOCAL).set("$ngcRepAdjust", faction.relAdjustMap, 7);
        List<String> raised = new ArrayList<>(10);
        List<String> lowered = new ArrayList<>(10);
        for (Entry<String, Float> entry : faction.relAdjustMap.entrySet()) {
            String otherId = entry.getKey();

            FactionChoice otherFaction = FactionChoice.getFactionChoice(otherId);
            if (otherFaction.req.isLoaded()) {
                float relAdjust = entry.getValue();
                if (relAdjust > 0f) {
                    raised.add(otherFaction.nameWithArticle);
                } else {
                    lowered.add(otherFaction.nameWithArticle);
                }
            }
        }

        CharacterCreationData data = (CharacterCreationData) memoryMap.get(MemKeys.LOCAL).get("$characterData");
        data.setStartingLocationName(faction.starSystemName);

        dialog.getTextPanel().setFontSmallInsignia();
        if (!raised.isEmpty()) {
            dialog.getTextPanel().addParagraph("Improved reputation with " + Misc.getAndJoined(raised.toArray(
                    new String[raised.size()])), Misc.getPositiveHighlightColor());
            dialog.getTextPanel().highlightInLastPara(Misc.getHighlightColor(), raised.toArray(new String[0]));
        }
        if (!lowered.isEmpty()) {
            dialog.getTextPanel().addParagraph("Decreased reputation with " + Misc.getAndJoined(lowered.toArray(
                    new String[lowered.size()])), Misc.getNegativeHighlightColor());
            dialog.getTextPanel().highlightInLastPara(Misc.getHighlightColor(), lowered.toArray(new String[0]));
        }
        dialog.getTextPanel().setFontInsignia();

        return true;
    }
}
