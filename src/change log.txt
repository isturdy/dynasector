Version 1.4.4 (Unreleased)
- Scavenger fleets failing to despawn are cleaned up periodically
- Fixed small bug with named bounty reports in Nexerelin
- Fix to debris field buildup, back-ported from Starsector 0.9a
- Updated for Scy
- Updated for Diable Avionics
- Updated for Outer Rim Alliance

Version 1.4.3 (January 26, 2018)
- Updated for Shadowyards
- Updated for Ship/Weapon Pack
- Updated for Tyrador Safeguard Coalition

Version 1.4.2 (January 10, 2018)
- Updated for Shadowyards

Version 1.4.1 (December 1, 2017)
- Added randomizer theming to Random Battle and Custom Battle
- Fixed rare profiler crash with capital-case memory options
- Improved theming for Scy and Shadowyards
- Minor fleet randomization enhancements
- Adjusted Interstellar Imperium fighter weights
- Certain factions have adjusted rates for Ballistic/Missile/Energy weapons in markets
- Updated for Ship/Weapon Pack
- Updated for Underworld
- Updated for Junk Pirates, PACK, and ASP Syndicate

Version 1.4.0 (October 1, 2017)
- Added support for Tyrador Safeguard Coalition
- factions.csv, weapon_categories.csv, ship_roles.csv, and fighter_wings.csv now merge with other mods that define them
- Fixed minor bugs
- Fixed occasional problem where slots might be filled with inappropriate weapons (such as annihilators on side mounts)
- Vain attempt to fix save instability
- Assassination bounties will only spawn for hostile factions
- Adjusted theming system for allied factions
- Updated for Ship/Weapon Pack
- Updated for Interstellar Imperium
- Updated for Shadowyards
- Updated for Diable Avionics
- Updated for Outer Rim Alliance

Version 1.3.2 (June 19, 2017)
- Removed unnecessary textures
- Fixed eternal war bug (from vanilla)
- Adjustments to person bounties, again

Version 1.3.1b (June 13, 2017)
- Fixed memory bitching

Version 1.3.1 (June 13, 2017)
- Fixed crash bugs
- Improved memory bitching

Version 1.3.0 (June 11, 2017)
- Removed ship randomization (individual factions can already accomplish this through faction.json editing)
- Fleet and variant randomization now uses a consistent random seed, so the results can't be easily re-rolled by saving and loading
- Significantly reduced baseline tech-mixing for variant generation
- Expanded fleet theme system, particularly for faction-tech themes
- Faction themes now depend on various world factors, such as lore of the market the fleet originates from (for example, Umbra is likely to spawn Diktat-themed pirates)
- Readjusted early game difficulty compensation measures
- Adjusted bounties to match new vanilla expectations
- Fixed SCY start
- Fixed LPC spawn rate in markets
- Bounties against mercenaries and deserters now have a low negative reputation impact
- Fixed bounty spawning, especially early bounties
- Various improvements to bounty reward calculation, including increased rewards for bounties far from the core worlds

Version 1.2.0 (June 5, 2017)
- Updated to support Starsector 0.8.1a
- Updated for Diable Avionics
- Updated for Shadowyards Reconstruction Authority
- Updated for Interstellar Imperium
- Updated for Knights Templar
- Updated for Ship/Weapon Pack
- Updated for Underworld
- Updated for SCY
- Updated for Blackrock Drive Yards
- Weapon matching / mirroring improved
- Improved slot-assigning priority for certain ships
- Support for fighter wings in randomized variants
- Improved hullmod handling in numerous ways
- Enhanced named bounties (feature transfer from Starsector+)
- Made Pather bounties slightly rarer and more aggressive
- Enhanced large vanilla fleet compositions (feature transfer from Starsector+)
- Improved Nexerelin compatibility
- Patrol fleets of captured markets now have variants incorporating some of the original faction's tech
- Submarkets of captured markets now sell equipment from the old faction's tech
- Improved new game dialog

Version 1.1.0 (April 11, 2017)
- Outer Rim Alliance integrated
- Code cleanup

Version 1.0.8 (April 7, 2017)
- Alliances in Nexerelin now influence variant creation by mixing faction tech
- Minor adjustments to variant archetype selection algorithm
- Bug fixes

Version 1.0.7 (March 26, 2017)
- Updated Imperium support
- Updated Underworld support
- Updated Templar support

Version 1.0.6 (February 6, 2017)
- Variants in the campaign are only generated when the player interacts with a fleet (memory and performance savings)
- Updated Blackrock support
- Updated Imperium support
- Updated Templar support
- Updated Diable support
- Updated Scy support
- Improved externalization of weapon details

Version 1.0.5 (January 8, 2017)
- Updated Imperium support
- Updated Scy support
- Updated Underworld support
- Various minor fixes

Version 1.0.4 (September 17, 2016)
- Adjusted pirate tech weights
- Exigency/Ahriman support update
- Tiandong support update
- General improvements
- Variant naming improvements

Version 1.0.3 (May 21, 2016)
- General compatibility update
- Faction table updates for THI and SWP

Version 1.0.2 (April 23, 2016)
- Fix weapon group duplication bug

Version 1.0.1 (April 22, 2016)
- Fix an ID change
- Improve weapon grouper system

Version 1.0.0 (April 20, 2016)
- Initial release (split from Starsector+)
- startOptions.json now called DYNASECTOR_OPTIONS.ini
- Improved fleet generation and variant randomizer to make faction weapon loadouts more thematic to the type of fleet being created
- Variant randomizer updated for new BRDY hullmod behavior
